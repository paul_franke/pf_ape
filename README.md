# Einleitung #

Hier werden in Zukunft Informationen zum Projekt zu finden sein.

### Für was ist das hier ? ###

* Grundsätzlich geht es hier um die App die wir im zuge des Projektes erstellen wollen, um eine gut Note In BP zu bekommen und noch etwas dazulernen zu können.

### Planung ###

* Für die Planung sind alle eingeladen die [Wiki](https://bitbucket.org/gamemonkeys/apegame/wiki/) zu bearbeiten und mit Ideen zu füllen.

### Tools ###

Hier könnt ihr weiter notwendige Tools hinterlegen.

Map & Level Editor

* http://www.mapeditor.org/

### Entwicklung ###

Hier kommen nur dinge hin die die Entwicklung selbst betreffen.

### Lokales Projekt einrichten ###
* Als erstes das LibGdx Projekt einrichten wie auf dem Bild zu sehen.
![upload.png](https://bitbucket.org/repo/L4rdK7/images/2915617499-upload.png)





* Als nächstes in IntelliJ den Git Client **abschalten**.

* Jetzt kann man mit dem Sourcetree-tool [Link](https://www.sourcetreeapp.com/) das Repository holen.


* [**Understand the Libgdx Framework**](http://www.kilobolt.com/day-3-understanding-the-libgdx-framework.html)

* [libGDX Tutorial (English)](http://www.gamefromscratch.com/page/LibGDX-Tutorial-series.aspx)

* [LinkedList Tutorial](http://crunchify.com/how-to-iterate-through-linkedlist-instance-in-java/)

* [Android Developer Help](http://developer.android.com/guide/index.html)

### Mitwirkende ###
 * Paul
 * Tony
 * Dennis
 * Roman