package com.gamemonkeys.ape;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		// Edit android config
		config.useImmersiveMode = true; // Hiermit wird der ganze bildschirm genutzt navbar / titlebar werden ausgeblendet SDK > 19
		config.useAccelerometer = false; // Spart akku
		config.useCompass = false; // auch das spart akku

		initialize(new Ape(), config);
	}
}
