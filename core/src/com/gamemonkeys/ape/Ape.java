package com.gamemonkeys.ape;

// Imports
import com.badlogic.gdx.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.gamemonkeys.ape.sources.*;
import com.gamemonkeys.ape.sources.math.cColor;
import com.gamemonkeys.ape.sources.math.cShape;
import com.uwsoft.editor.renderer.SceneLoader;
import com.uwsoft.editor.renderer.utils.ItemWrapper;

/**
 * @author Paul
 * @version 0.0.6
 */

public class Ape extends Game {
    /** Haupt methode zum erstellen des Spiels */

    private cStateMachine oStateMachine;

    @Override
    public void create() {

        /**
         Spiel setup 'Flying Rects'
         */

        // Debug
        Gdx.app.log("Ape->create","Called");

        /**
         1. Um dem neuen State basirendem Konzept zu entsprechen muss als erstes die StateMachine erstellt werden.
         Ihr wird die instanz der Game Klasse übergeben. Da unsere Klasse Ape selbst die Klasse Game implementiert
         kann hier einfach this übergeben werden.
         */
        this.oStateMachine = new cStateMachine(this);

        // Für Testzwecke
        // https://github.com/libgdx/libgdx/wiki/Continuous-&-non-continuous-rendering
        // Gdx.graphics.setContinuousRendering(false);

        /**
         2. Jetzt werden die State´s erstellt. jeder State erhält einen eindeutigen Namen.
         Jeder Name darf nur einmal vorkommen (Groß und Kleinschreibung wird ignoriert also Menu == menu).
         Hier wird auch die create Methode des States aufgerufen
         */
        this.oStateMachine.createState("Game_Menu", new cApeMenu());
        this.oStateMachine.createState("Test", new cTestIngame());

        /**
         3. Nun muss nur noch der State aktiviert werden
         */
        this.oStateMachine.activateState("Game_Menu");

    }

    @Override
    public void dispose() {

        // Debug
        Gdx.app.log("Ape->dispose","Called");

        // Hiermit wir alles gelöscht
        this.oStateMachine.dispose();

        // Dispose der übergeordneten Klasse aufrufen
        super.dispose();

    }
}

/**
 * @author Tony
 *
 * Todo:
 * - Bei State änderung durch die StateMachine muss die Camera position zurück gesetzt werden
 * - Einen 2ten object type anlegen der von der cObject klasse erbt, für die Map objekte (cMapObject)
 * - Eine UI - Klasse einfügen, um eingaben abzufragen und buttons zu erstellen ... (menüs ... )
 *
 */
class cApeMenu extends cState {
    /** Haupt methode zum erstellen des Spiels */

    public Vector2 vGravity = new Vector2(0.0f, 9.88f);
    public cPlayer player;
    public cLevelSystem oLevelSystem;
    public cLevel oTestLevel;

    @Override
    public void create() {
        Gdx.app.log("cApeMenu->onCreate", "Called");

        // Create World, Renderer and Screen
        this.oStateWorld = new cWorld(new cShape(266, 160));
        this.oStateRenderer = this.oStateWorld.getRenderer();
        this.oStateRenderer.init();

        // Set Background color
        this.oStateRenderer.setBackgroundColor(cColor.COLOR_LIGHT_BLUE);

        // Set the InputProcessor
        Gdx.input.setInputProcessor(this.oStateWorld.getInput());

        // Load test Level
        this.oLevelSystem = new cLevelSystem(this.oStateRenderer);
        this.oTestLevel = this.oLevelSystem.Loadlevel("MainScene");

        // Get The player sheep
        player = new cPlayer(this.oTestLevel);
        player.setMovingSpeed(60f);
        this.oTestLevel.addScript("player", player);


    }

    @Override
    public void pause() {
        Gdx.app.log("cApeMenu->pause", "Called");
    }

    @Override
    public void show() {
        Gdx.app.log("cApeMenu->show", "Called");
    }

    @Override
    public void hide() {
        // Debug
        Gdx.app.log("cApeMenu->hide", "Called");
    }

    @Override
    public void resume() {
        // Debug
        Gdx.app.log("cApeMenu->resume", "Called ["+this.sName+"]");

        // Nach dem resume müssen wir den Input Processor wieder setzen
        //Gdx.input.setInputProcessor(this.oStateWorld.getInput());

    }

    @Override
    public void render(float delta) {

        // Schaltet den State um
        if (this.oStateWorld.buttonDown(Input.Keys.ESCAPE)) {
            this.switchState("Test");
        }

        // Update
        this.oStateWorld.update(delta);

        // Render
        this.oStateRenderer.cls();

        // Render Things
        this.oLevelSystem.update(delta);
        this.oStateRenderer.getCamera().position.x = this.player.getX();
        //   this.oStateRenderer.oViewport.getCamera().position.y =25 + this.player.getY();

        // Finish rendering
        this.oStateRenderer.flip(delta);

    }

    @Override
    public void resize(int width, int height) {

        // Ruft die Renderer methode resize auf
        this.oStateRenderer.resize(width, height);

        // Debug
        Gdx.app.log("cApeMenu->resize", "Called");
    }

    @Override
    public void dispose() {

        // Löscht die welt
        this.oStateWorld.dispose();

        // Debug
        Gdx.app.log("cApeMenu->dispose", "Called");
    }

}

/**
 * @author Tony
 */
class cTestIngame extends cState {

    @Override
    public void create() {
        Gdx.app.log("cTestIngame->create", "Called");

        // Create World, Renderer and Screen
        this.oStateWorld = new cWorld(new cShape(266, 160));
        this.oStateRenderer = this.oStateWorld.getRenderer();
        this.oStateRenderer.init();

        // Set Background color
        this.oStateRenderer.setBackgroundColor(cColor.COLOR_ORANGE);

        // Create test
        cFlyingBox Test = new cFlyingBox(new cShape(100,100), new cShape(50));
        this.oStateWorld.addObject(Test);

    }

    @Override
    public void pause() {
        Gdx.app.log("cTestIngame->pause", "Called");
    }

    @Override
    public void show() {
        Gdx.app.log("cTestIngame->show", "Called");

        // Reset Camera
        this.oStateRenderer.getCamera().position.set(266f/2f, 160f/2f, 0.0f);
    }

    @Override
    public void hide() {
        // Debug
        Gdx.app.log("cApeMenu->hide", "Called");
    }

    @Override
    public void resume() {
        // Debug
        Gdx.app.log("cApeMenu->resume", "Called ["+this.sName+"]");
    }

    @Override
    public void render(float delta) {

        // Schaltet den State um
        if (this.oStateWorld.buttonDown(Input.Keys.ESCAPE)) {
            this.switchState("Game_Menu");
        }

        // Update
        this.oStateWorld.update(delta);

        // Update objects
        this.oStateWorld.updateObj(delta);

        // Render
        this.oStateRenderer.cls();
        this.oStateRenderer.flip(delta);

        // Debug
        //Gdx.app.log("cApeMenu->render", "Called");
    }

    @Override
    public void resize(int width, int height) {

        // Ruft die Renderer methode resize auf
        this.oStateRenderer.resize(width, height);

        // Debug
        Gdx.app.log("cApeMenu->resize", "Called");
    }

    @Override
    public void dispose() {
        this.oStateWorld.dispose();
        Gdx.app.log("cTestIngame->dispose", "Called");
    }

}