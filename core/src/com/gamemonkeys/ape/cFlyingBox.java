package com.gamemonkeys.ape;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.gamemonkeys.ape.sources.cObject;
import com.gamemonkeys.ape.sources.cWorld;
import com.gamemonkeys.ape.sources.math.cColor;
import com.gamemonkeys.ape.sources.math.cShape;

import java.util.Random;

/**
 * @author Paul
 * @version 0.0.4
 */

public class cFlyingBox extends cObject  {
    /**
     * Ist eine kleine Testklasse um bewegliche objekte zu demonstrieren
     */
    int iSpeed;
    int iStartBound;

    /** Konstruktor: Erstellt eine neue Box */
    public cFlyingBox (cShape oStartPosition, cShape oSize) {

        // Set Start Size and Position
        this.oPosition = oStartPosition;
        this.oSize = oSize;

        // Random
        Random rnd = new Random(System.currentTimeMillis() % 1000);

        // Set Speed
        this.iSpeed = MathUtils.random(50, 100);
        this.iStartBound = MathUtils.random(1, 4);
    }

    /** Updatet das objekt */
    @Override
    public void update(float fDelta, int iWidth, int iHeight) {

        /*
            Hier wird gezeigt wie eine Abfrage der maus stattfinden kann
         */
        if (this.oWorld.mouseDown(0) == true) {
            // 0 = Linke Maus taste auf geräten wo keine maus angeschlossen ist, ist die 0 die erste berührung mit dem
            // touchscreen
            Gdx.app.log("cFlyingBox->update", "Mouse Pos : " + this.oWorld.getMouseX() + " x " + this.oWorld.getMouseY());
        }


        // Trys
        int iTrys = 0;

        // Update Pos
        switch (this.iStartBound) {
            case 1: // Left -> Right
                this.oPosition.fWidth=this.oPosition.fWidth+this.iSpeed*fDelta;

                // Check the screen boundarys
                if (this.oPosition.fWidth > iWidth) {
                    this.oPosition.fWidth = 0 - this.oSize.fWidth*2;
                    iTrys++;
                }

                break;
            case 2: // Up -> Down
                this.oPosition.fHeight=this.oPosition.fHeight+this.iSpeed*fDelta;

                // Check the screen boundarys
                if (this.oPosition.fHeight > iHeight) {
                    this.oPosition.fHeight = 0 - this.oSize.fHeight*2;
                    iTrys++;
                }

                break;
            case 3: // Right -> Left
                this.oPosition.fWidth=this.oPosition.fWidth-this.iSpeed*fDelta;

                // Check the screen boundarys
                if (this.oPosition.fWidth+this.oSize.fWidth < 0) {
                    this.oPosition.fWidth = iWidth + this.oSize.fWidth*2;
                    iTrys++;
                }

                break;
            case 4: // Down -> Up
                this.oPosition.fHeight=this.oPosition.fHeight-this.iSpeed*fDelta;

                // Check the screen boundarys
                if (this.oPosition.fHeight+this.oSize.fHeight < 0) {
                    this.oPosition.fHeight = iHeight + this.oSize.fHeight*2;
                    iTrys++;
                }

                break;
        }

        // Check Trys
        if (iTrys > 4) {
            this.iSpeed = MathUtils.random(10, 25);
            this.iStartBound = MathUtils.random(1, 4);
        }

    }

    /** Zeichnet es */
    @Override
    public void render(ShapeRenderer oShape, SpriteBatch oSprite) {

        // Tells shapeRenderer to begin drawing filled shapes
        oShape.begin(ShapeRenderer.ShapeType.Filled);

        // Chooses RGB Color of 87, 109, 120 at full opacity
        oShape.setColor(cColor.COLOR_BROWN.toColor());

        // Draws the rectangle from myWorld (Using ShapeType.Filled)
        oShape.rect(this.oPosition.getWidth(), this.oPosition.getHeight(),
                    this.oSize.getWidth(),    this.oSize.getHeight() * this.oWorld.getScreen().getAspectRatio());


        oShape.end();

        // Set Blend ALPHA
        // Gdx.gl.glEnable(GL20.GL_BLEND);
        // Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        //Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    /** Löscht den speicher (wäre hier obsolet da keinen eigenen objekte erzeugt werden die gelöscht werden müssen */
    @Override
    public void dispose() {
        // Wird wird deswegen nur die übergeordnete klassenmethode aufgerufen
        super.dispose();
    }

}
