package com.gamemonkeys.ape;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.gamemonkeys.ape.sources.cLevel;
import com.uwsoft.editor.renderer.components.DimensionsComponent;
import com.uwsoft.editor.renderer.components.TransformComponent;
import com.uwsoft.editor.renderer.physics.PhysicsBodyLoader;
import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ComponentRetriever;

/**
 * @author Tony
 * @version 0.0.1
 *
 * Die Klasse könnte noch mit in den core source übernommen werden, wenn sie allgemeiner wird
 */
public class cPlayer implements IScript {
    /** Einfache klasse für das Player Object */

    private World oWorld;
    private Entity oPlayer;
    private TransformComponent transform;
    private DimensionsComponent dimension;
    private Vector2 fSpeed = new Vector2(10f, 0f);
    private float fGravity = -950f;
    private float fJumpSpeed = 300f;
    private boolean bInAir = false;

    public cPlayer(cLevel oLevel) {
        this.oWorld = oLevel.getPhysicsWorld();
    }

    public void setMovingSpeed(float fSpeed) {
        this.fSpeed.x = fSpeed;
    }

    public float getX() {
        return this.transform.x;
    }
    public void setX(float X) {
        this.transform.x = X;
    }

    public float getY() {
        return this.transform.y;
    }
    public void setY(float Y) {
        this.transform.y = Y;
    }

    public float getWidth() {
        return this.dimension.width;
    }

    public float getHeight() {
        return this.dimension.height;
    }

    @Override
    public void init(Entity entity) {
        this.oPlayer = entity;
        this.transform = ComponentRetriever.get(this.oPlayer, TransformComponent.class);
        this.dimension = ComponentRetriever.get(this.oPlayer, DimensionsComponent.class);
    }

    @Override
    public void act(float delta) {

        // Check Keys
        if ((Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.getDeltaX() < 0)) {
            this.transform.x-=fSpeed.x*delta;
            this.transform.scaleX=-1f;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.getDeltaX() > 0) {
            this.transform.x+=fSpeed.x*delta;

            this.transform.scaleX=1f;
        }

        // Jump
        if ((Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.isTouched(1)) && this.bInAir == false) {
            this.fSpeed.y = fJumpSpeed;
            this.bInAir = true;
        }

        // Calculate the gravity
        this.fSpeed.y += this.fGravity * delta;
        this.transform.y += this.fSpeed.y * delta;

        // Check Collision
        CheckRaycasting();

    }

    private void CheckRaycasting() {

        // Position des Rays nur y achse
        float rayGap = this.dimension.height/2;

        // Die länge des Rays (Strahls) die nach unten zeigt
        float raySize = -(this.fSpeed.y + Gdx.graphics.getDeltaTime()) * Gdx.graphics.getDeltaTime();

        // Check
        if (raySize < 5f) raySize = 5f;

        // only check for collision when moving down
        if (this.fSpeed.y > 0) return;

        // The Vector position for the ray From and To
        Vector2 rayFrom = new Vector2((this.getX()+this.getWidth()/2) * PhysicsBodyLoader.getScale(),
                (this.getY()+rayGap) * PhysicsBodyLoader.getScale());

        Vector2 rayTo = new Vector2((this.getX()+this.getWidth()/2) * PhysicsBodyLoader.getScale(),
                (this.getY()-raySize) * PhysicsBodyLoader.getScale());

        // Raycast
        oWorld.rayCast(new RayCastCallback() {
            @Override
            public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {

                // Stop the player
                fSpeed.y = 0;

                // Der player muss etwas über dem punkt der kollision gesetzt werden da es sonst zu fehlern kommen kann
                setY(point.y /  PhysicsBodyLoader.getScale() + 0.1f);

                // make sure it is grounded, to allow jumping again
                bInAir = false;

                return 0;
            }
        }, rayFrom, rayTo);


    }

    @Override
    public void dispose() {
        Gdx.app.log("cPlayer->dispose", "Called");
    }
}
