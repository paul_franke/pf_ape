package com.gamemonkeys.ape.sources;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

/**
 * @author Paul
 * @version 0.0.8
 */
public class cInput implements InputProcessor {
    /** Fängt die systemeingaben ab */

    // Statische
    public static final short MAX_MOUSE_BUTTONS = 12;

    // Attribute
    public int iMouseX, iMouseY;
    public byte bScroll;
    public char cLastKey;
    public boolean[] baMouseButtonDown;
    public boolean[] baKeyDown;
    public boolean[] baMouseButtonUp;
    public boolean[] baKeyUp;

    /** Konstruktor : Erstellt die cInput klasse */
    public cInput() {

        // Setzt das Mousebutton array
        // maximal 12 buttons auf der maus werden unterstützt
        this.baMouseButtonDown = new boolean[MAX_MOUSE_BUTTONS];
        this.baMouseButtonUp = new boolean[MAX_MOUSE_BUTTONS];

        // Es kann auf einem normalen Keyboard nur 255 Tasten geben mit sondertasten
        this.baKeyDown = new boolean[256];
        this.baKeyUp = new boolean[256];

        // Debug
        Gdx.app.log("cInput->Constructor", "Initialised");
    }

    /** Key / Maus und Touch eingaben abfangen */
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        this.baMouseButtonDown[button] = true;

        /**
         * Bei IOS & Android muss auch die Position der Eingabe gesetzt werden, da die Position
         * nicht dauerhaft aktualisiert wird mit @mouseMoved
         * */
        if (Gdx.app.getType()== Application.ApplicationType.Android ||
            Gdx.app.getType()== Application.ApplicationType.iOS) {
            this.iMouseX = screenX;
            this.iMouseY = screenY;
        }

        return false;
    }

    @Override
    public boolean keyDown(int keycode) {

        this.baKeyDown[keycode] = true;
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {

        // Set mouse position
        this.iMouseX = screenX;
        this.iMouseY = screenY;

        return false;
    }

    @Override
    public boolean scrolled(int amount) {

        /**
         * Wir invertieren hier die zahl um das scrollen in die richtige richtung auszuführen
         * weil das Koordinatensystem auf left down steht.
         */
        this.bScroll = (byte)(amount * -1);

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        this.baMouseButtonUp[button] = true;
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        this.baKeyUp[keycode] = true;
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        this.cLastKey = character;
        return false;
    }

    /** Löscht alle gespeicherten eingaben, es würde sonst zu Problemn bei abfragen kommen */
    public void ClearInput() {

        // Clear all
        this.bScroll = 0;
        this.cLastKey = 0;

        // Clear keyboard array
        for (int idx = 0; idx < 256; idx++) {
            this.baKeyUp[idx] = false;
            this.baKeyDown[idx] = false;
        }

        // Clear Mouse buttons
        for (int idx = 0; idx < cInput.MAX_MOUSE_BUTTONS; idx++) {
            this.baMouseButtonUp[idx] = false;
            this.baMouseButtonDown[idx] = false;
        }

    }

    /** Löscht alles aus dem speicher */
    public void dispose() {
        this.baKeyDown = null;
        this.baKeyUp = null;
        this.baMouseButtonDown = null;
        this.baMouseButtonUp = null;
    }

    /** Nicht benutzt */
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

}
