package com.gamemonkeys.ape.sources;

import com.badlogic.gdx.physics.box2d.World;
import com.uwsoft.editor.renderer.SceneLoader;
import com.uwsoft.editor.renderer.data.SceneVO;
import com.uwsoft.editor.renderer.scripts.IScript;
import com.uwsoft.editor.renderer.utils.ItemWrapper;

/**
 * @author Paul
 * @version 0.0.1
 */
public class cLevel {
    /**
     *
     */
    private SceneVO oScene;
    private SceneLoader oLevelSystem;
    private cRenderer oRenderer;
    private String sSceneName;
    private ItemWrapper oRootNode;

    /** Konstruktor : Erstellt das objekt */
    public cLevel(SceneLoader oSystem, cRenderer oRenderer, String sSceneName) {

        // Set attributes
        this.oLevelSystem = oSystem;
        this.oRenderer = oRenderer;
        this.sSceneName = sSceneName;

        // Load the scene
        this.oScene = this.oLevelSystem.loadScene(this.sSceneName,this.oRenderer.getViewport());

        // Holt die Objekte aus der Map um sie zu ändern oder zu bewegen
        this.oRootNode = new ItemWrapper(this.oLevelSystem.getRoot());
    }

    /** Gibt die Physikalische welt zurück */
    public World getPhysicsWorld() {
        return this.oLevelSystem.world;
    }

    public void addScript(String sKey, IScript oValue) {

        if (this.oRootNode!=null) {
            ItemWrapper child = this.oRootNode.getChild(sKey);

            if (child!=null) {
                child.addScript(oValue);
            }
        }
    }



}
