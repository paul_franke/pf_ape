package com.gamemonkeys.ape.sources;

import com.badlogic.gdx.Gdx;
import com.uwsoft.editor.renderer.SceneLoader;
import java.util.LinkedList;



/**
 * @author Tony
 * @version 0.0.1
 */
public class cLevelSystem {
    /**
     * Diese Klasse stellt alle funktionen zur verfügung die Benötigt werden um ein Level und deren Objekte zu Laden
     */
    private SceneLoader oLoader;
    private cRenderer oRenderer;

    /** Wird noch nicht genutzt */
    private LinkedList<cLevel> lLoadedLevels;

    public cLevelSystem(cRenderer oRenderer) {

        // Create the scene loader
        this.oLoader = new SceneLoader();
        this.oRenderer = oRenderer;

    }

    /** Läd ein level und gibt dieses zurück */
    public cLevel Loadlevel(String sSceneName) {

        cLevel tmpLevel = new cLevel(this.oLoader, this.oRenderer, sSceneName);

        if (tmpLevel!=null) {
            return tmpLevel;
        } else {
            Gdx.app.error("cLevelSystem->Loadlevel" ,"Level konnte nicht geladen werden");
        }

        return null;
    }

    /** Gibt den SceneLoader zurück */
    public SceneLoader getLoader() {
        return this.oLoader;
    }

    /** Updatet die scenen */
    public void update(float fDelta) {
        this.oLoader.getEngine().update(fDelta);
    }

}
