package com.gamemonkeys.ape.sources;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.gamemonkeys.ape.sources.math.cColor;
import com.gamemonkeys.ape.sources.math.cShape;

/**
 * @author Paul
 * @version 0.0.1
 */
abstract public class cObject {
    /**
     * Ist die Basis für alle Objekte im spiel
     */

    /**
     * Basic Attributes
     */
    public String sName = "Default";           // if needed a name for this object

    public cShape oPosition = new cShape();    // Holds the actual Screen position
    public cShape oSize = new cShape();        // Holds the actual Size of the Object

    public cColor oBaseColor = cColor.COLOR_WHITE.copy();
    public boolean bUseAlpha = false;

    public cWorld oWorld;

    /**
     * Standardklasse zum setzen der Welt
     */
    public void setWorld(cWorld oWorld) {
        this.oWorld = oWorld;
    }

    /**
     * Methods to Overload
     */
    abstract public void update(float fDelta, int iWidth, int iHeight);
    abstract public void render(ShapeRenderer oShape, SpriteBatch oSprite);
    public void dispose() {
        this.oPosition = null;
        this.oSize = null;
        this.oBaseColor = null;
    }
}
