package com.gamemonkeys.ape.sources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.*;
import com.gamemonkeys.ape.sources.math.cColor;
import com.sun.glass.ui.View;

import java.util.ListIterator;

/**
 * @author Tony
 * @version 0.1.0
 */
public class cRenderer {
    /**
     * Zeichnet am ende alles auf dem Bildschirm
     */

    // Attributes
    private cScreen oScreen;
    private cWorld oWorld;
    private Viewport oViewport;

    private OrthographicCamera oCamera;

    private SpriteBatch oBatch;
    private ShapeRenderer oShape;

    private FreeTypeFontGenerator oFontGen;
    private FreeTypeFontGenerator.FreeTypeFontParameter oFontParam;
    private BitmapFont oFontBig;
    private BitmapFont oFontNormal;
    private BitmapFont oFontSmall;

    private cColor oBackgroundColor = new cColor();

    /** Konstruktor : Initialisiert das Object (new cRenderer(...) */
    public cRenderer(cWorld oWorld) {

        // Check world
        this.setWorld(oWorld);

        // Set the screen
        this.oScreen = this.oWorld.getScreen();

        // Log
        Gdx.app.log("cRenderer", "Initialised");

    }

    /** Initialisiert die Wichtigen elemente für den Renderer (Cam, SpriteRenderer, Batch, ...) */
    public void init() {

        // Check Screen
        if (this.oScreen == null) {
            Gdx.app.error("cRenderer", "Screen not initialised !");
            return;
        }

        // Create the camera
        this.oCamera = new OrthographicCamera();
        //this.oCamera.setToOrtho(true);

        // Create the Viewport
        this.oViewport = new FitViewport(this.oScreen.getShape().fWidth , //* this.oScreen.getAspectRatio(),
                                         this.oScreen.getShape().fHeight, this.oCamera);
        this.oViewport.apply();

        this.oCamera.position.set(this.oScreen.getShape().fWidth/2, this.oScreen.getShape().fHeight/2, 0f);

        // Create the shape Renderer
        this.oShape = new ShapeRenderer();
        this.oShape.setProjectionMatrix(this.oCamera.combined);

        // Create the Batch
        this.oBatch = new SpriteBatch();
        this.oBatch.setProjectionMatrix(this.oCamera.combined);

        // Load and set the default Font
        this.oFontGen = new FreeTypeFontGenerator(Gdx.app.getFiles().internal("fonts/Roboto-Bold.ttf"));
        this.oFontParam = new FreeTypeFontGenerator.FreeTypeFontParameter();
        this.oFontParam.flip = true;

        // Generate Big sized
        this.oFontParam.size = 24;
        this.oFontBig = this.oFontGen.generateFont(this.oFontParam);

        // Generate Normal sized
        this.oFontParam.size = 12;
        this.oFontNormal = this.oFontGen.generateFont(this.oFontParam);

        // Generate Small sized
        this.oFontParam.size = 6;
        this.oFontSmall = this.oFontGen.generateFont(this.oFontParam);

    }

    /** Setzt die Screen instanz für den Renderer */
    public void setScreen(cScreen oScreen) {
        // Check
        if (oScreen==null) {
            Gdx.app.error("cRenderer", "Screen not created !", new NullPointerException());
        }

        // Set it
        this.oScreen = oScreen;
    }

    /** Setzt die Welt instanz für den Renderer */
    public void setWorld(cWorld oWorld) {
        // Check world
        if (oWorld == null){
            Gdx.app.error("cRenderer", "World not created !", new NullPointerException());
        }

        // Set it
        this.oWorld = oWorld;
    }

    /** Setzt die Hintergrundfarbe */
    public void setBackgroundColor(cColor oValue) {
        this.oBackgroundColor.setColor(oValue);
    }

    /** Holt die Hintergrundfarbe */
    public cColor getBackgroundColor() {
        return this.oBackgroundColor;
    }

    /** Löscht den Bildschirm, setzt die hintergrundfarbe und führt ein update für die Camera durch*/
    public void cls() {

        // Set the background color
        Gdx.gl.glClearColor(this.oBackgroundColor.getRed(), this.oBackgroundColor.getGreen(),
                            this.oBackgroundColor.getBlue(), this.oBackgroundColor.getAlpha());

        // Clear the screen
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Update
        this.oCamera.update();
    }

    /** Zeigt alle objekte und die States auf dem Bildschirm an*/
    public void flip(float delta) {

        // Render all Objects
        this.renderObj();

    }

    /** Rendert alle Objekte */
    public void renderObj() {

        // List Iterator
        ListIterator<cObject> listEnum = this.oWorld.getList().listIterator();

        // Check, Liste ist null
        if (listEnum == null)
            return;

        // Update All elements
        while (listEnum.hasNext()) {

            // Get Object
            cObject objTMP = listEnum.next();

            // check if ref is not null
            if (objTMP != null) {
                objTMP.render(this.oShape, this.oBatch);
            }

        }
    }

    /** Gibt das Spritebatch zurück für Grafiken */
    public SpriteBatch getBatch() {
        this.oBatch.setProjectionMatrix(this.oCamera.combined);
        return this.oBatch;
    }

    /** Gibt den ShapeRenderer zurück für einfache Zeichen (Rect...) */
    public ShapeRenderer getShape() {
        this.oShape.setProjectionMatrix(this.oCamera.combined);
        return this.oShape;
    }

    /** Gibt den Viewport zurück */
    public Viewport getViewport() {
        return this.oViewport;
    }

    /** Gibt die Camera zurück */
    public Camera getCamera() {
        return this.oCamera;
    }

    /** Wird bei ändereungen des Bildschirms aufgerufen*/
    public void resize(int iWidth, int iHeight ) {

        // Prüfen ob screen erstellt wurde
        if (this.oViewport != null) {

            // Update the viewport
            this.oViewport.update(iWidth, iHeight);

            // Setup the camera
            this.oCamera.position.set(this.oScreen.getShape().fWidth/2, this.oScreen.getShape().fHeight/2, 0f);

            // Debug
            // Gdx.app.log("cRenderer->resize",this.oScreen.getShape().toString());
        }
    }

    /** Löscht alle unnötigen referenzen aus dem speicher */
    public void dispose() {

        // Clear up
        this.oCamera = null;
        this.oBatch.dispose();
        this.oShape.dispose();
        this.oFontGen.dispose();
        this.oFontParam = null;
        this.oFontBig.dispose();
        this.oFontNormal.dispose();
        this.oFontSmall.dispose();

    }
}
