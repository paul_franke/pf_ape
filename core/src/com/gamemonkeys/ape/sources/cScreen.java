package com.gamemonkeys.ape.sources;

import com.badlogic.gdx.Gdx;

import com.gamemonkeys.ape.sources.math.*;

/**
 * @author Paul
 * @version 0.1.2
 */

public class cScreen{
    /**
     * Zeichnet alles auf einen bildschirm und hält instanzen von der World und dem Renderer
     */

    // Attributes
    private float fAspectRatio = 0.0f;     // Verhältnis zwischen Breite / Höhe
    private cShape oScreen;

    /** Konstruktor : Initialisiert das Object (new cScreen(...) */
    public cScreen (cShape oScreenSize){

        // Init Shape
        if (oScreenSize!=null) {
            this.oScreen = oScreenSize;
        } else {
            this.oScreen = new cShape(100, 50);
            Gdx.app.log("cScreen->Constructor", "Shape is Null, use default [100, 50] !");
        }

        // Log
        Gdx.app.log("cScreen->Constructor", "Initialised");

    }

    /** Berechnet erst das Seitenverhältnis (fHeight / fWidth) und gibt es dann zurück */
    public float getAspectRatio() {

        // Calculate
        this.fAspectRatio = this.oScreen.fHeight / this.oScreen.fWidth;

        // Debug
        //Gdx.app.log("cScreen->getAspectRatio", "Aspect : " + this.fAspectRatio);

        // Return
        return this.fAspectRatio;

    }

    public cShape getShape() {
        return this.oScreen;
    }

    /** Setzt das Shape für die Auflösung */
    public void setScreenSize(float fWidth, float fHeight) {

        // Set the Shape if it is not null
        if (this.oScreen !=null) {
            this.oScreen.setShape(fWidth, fHeight);
        } else {
            this.oScreen = new cShape(fWidth, fHeight);
        }

        // Calculate the AspectRatio
        this.getAspectRatio();
    }

    /** Löscht alles aus dem speicher */
    public void dispose() {
        this.oScreen = null;
    }
}
