package com.gamemonkeys.ape.sources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

/**
 * @author Tony
 * @version 0.0.1
 */
public abstract class cState implements Screen {
    /**
     * Definiert die Hauptstruktur der States
     */
    public String sName;
    public cWorld oStateWorld;
    public cRenderer oStateRenderer;
    public cStateMachine oStateM;

    public abstract void create();

    /**
     * Ändern den aktuellen State in der State Machine
     * @param sName & oState geben die states an, in die gewechselt werden soll
     */
    public final void switchState(String sName) {
        this.oStateM.activateState(sName);
    }
    public final void switchState(cState oState) {
        this.oStateM.activateState(oState);
    }

    @Override
    public abstract void pause();

    @Override
    public abstract void show();

    @Override
    public abstract void hide();

    @Override
    public abstract void resume();

    @Override
    public abstract void render(float delta);

    @Override
    public abstract void resize(int width, int height);

    @Override
    public abstract void dispose();

}
