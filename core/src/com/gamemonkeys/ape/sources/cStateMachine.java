package com.gamemonkeys.ape.sources;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * @author Tony
 * @version 0.0.1
 */
public class cStateMachine {
    /**
     * Definiert die Hauptstruktur der StateMachine
     */
    private Game oMainContext;
    private LinkedList<cState> lStateList;
    private cState oCurrentActiveState;

    /** Konstruktor : Erstellt die State Machine */
    public cStateMachine(Game oContext) {

        // Check
        if (oContext == null) {
            Gdx.app.error("cStateMachine->Constructor", "Game context not found !", new NullPointerException());
        }

        // Set the Context
        this.oMainContext = oContext;

        // Create list
        this.lStateList = new LinkedList<cState>();

    }

    /** Erstellt einen State und fügt ihn der internen Liste hinzu */
    public void createState(String sName, cState oState) {

        // Check State exist
        if (oState == null) {
            Gdx.app.error("cStateMachine->createState", "State not exist", new NullPointerException());
        }

        // Prüfen ob der name in der liste bereits vorhanden ist
        if (this.getState(sName) == null) {

            // Namen des cStates setzen
            oState.sName = sName;
            oState.oStateM = this;

            // element zur liste hinzufügen
            this.lStateList.addLast(oState);

            // Ausführen der onCreate Methode
            oState.create();

        } else {
            // Der cState mit dem sName existiert bereits
            Gdx.app.error("cStateMachine->createState", "State with name :"+sName+" already exists !");
            return;
        }


    }

    /** Sucht einen cState nach dem namen, Groß und Klein-schreibung werden ignoriert*/
    public cState getState(String sName) {

        // List Iterator
        ListIterator<cState> listEnum = this.lStateList.listIterator();

        // Check, Liste ist null
        if (listEnum == null)
            return null;

        // Search for object
        while (listEnum.hasNext()) {

            // Get Object
            cState objTMP = listEnum.next();

            // check if ref is not null
            if (objTMP != null && objTMP.sName.equalsIgnoreCase(sName)) {
                return objTMP;
            }
        }

        // Not found in the list
        return null;

    }

    /**
     * Aktiviert den angegebenen state
     * @param sName der State der dem sName entspricht wird aktiviert
     * Führt die Methoden onPause für den alten State und onResume für den neuen State aus
     */
    public void activateState(String sName) {

        // Suchen des States
        cState tmpState = this.getState(sName);

        // Checken ob der State gefunden wurde
        if (tmpState != null) {
//            if (this.oCurrentActiveState!=null)
//                this.oCurrentActiveState.pause();

            this.oCurrentActiveState = tmpState;
          //  this.oCurrentActiveState.resume();

            // Switch screen if exist
            if (this.oCurrentActiveState != null) {
                this.oMainContext.setScreen(this.oCurrentActiveState);
            }

        } else {
            Gdx.app.error("cStateMachine->activateState","State with name :"+sName+" not found !");
        }

    }

    /**
     * Aktiviert den angegebenen state
     * @param oState hier wird der State direkt aktiviert wenn er existiert
     * Führt die Methoden onPause für den alten State und onResume für den neuen State aus
     */
    public void activateState(cState oState) {

        // Prüfen ob der State existiert
        if (oState == null) {
            Gdx.app.error("cStateMachine->activateState", "State not exist !", new NullPointerException());
        }

        // Den state als aktiv setzen
       // if (this.oCurrentActiveState!=null)
        //    this.oCurrentActiveState.onPause();

        this.oCurrentActiveState = oState;
        //this.oCurrentActiveState.onResume();

        // Switch screen if exist
        if (this.oCurrentActiveState != null) {
            this.oMainContext.setScreen(this.oCurrentActiveState);
        }

    }

    /** Sucht den State und löscht ihn aus der liste, führt die onClose methode des States aus */
    public void removeState(String sName) {

        // Suchen des States
        cState tmpState = this.getState(sName);

        // Checken ob der State gefunden wurde
        if (tmpState != null) {
            this.lStateList.remove(tmpState);
          //  tmpState.onClose();
        } else {
            Gdx.app.error("cStateMachine->removeState","State with name :"+sName+" not found !");
        }

    }

    /** Löscht den State aus der liste und führt die onClose methode des States aus */
    public void removeState(cState oState) {

        // Prüfen ob der State existiert
        if (oState == null) {
            Gdx.app.error("cStateMachine->removeState", "State not exist !", new NullPointerException());
        }

        // Die methode aufrufen und dann aus der Liste entfernen
        //oState.onClose();
        this.lStateList.remove(oState);

    }

    /** Gibt den Game context zurück */
    public Game getContext() {
        return this.oMainContext;
    }

    /** Dispose : Wenn das object gelöscht wird */
    public void dispose() {
        // erst wird die Liste von allen objecten befreit und dann der speicher gelöscht = null
        this.lStateList.clear();
        this.lStateList = null;
    }

}

