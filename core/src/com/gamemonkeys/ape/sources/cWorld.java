package com.gamemonkeys.ape.sources;

import com.badlogic.gdx.Gdx;
import com.gamemonkeys.ape.sources.math.cShape;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 * @author Tony
 * @version 0.0.10
 */
public class cWorld {
    /**
     * Hält alle informationen über die 'Welt'
     */

    // Attributes
    private cScreen oScreen;
    private cRenderer oRenderer;
    private static cInput oInput;

    // List
    private LinkedList<cObject> lObjectsList;

    /** Konstruktor : Erstellt die 'Welt'*/
    public cWorld(cShape oValue) {

        // Create the new Screen
        this.oScreen = new cScreen(oValue);

        if (this.oScreen == null)
            Gdx.app.error("cWorld", "Could not create Screen !", new NullPointerException());

        // Create the renderer
        this.oRenderer = new cRenderer(this);

        if (this.oRenderer == null)
            Gdx.app.error("cWorld", "Could not create Renderer !", new NullPointerException());

        // Create the input processor and set them active
        if (this.oInput == null)
            this.oInput = new cInput();

        if (this.oInput == null)
            Gdx.app.error("cWorld", "Could not create Input !", new NullPointerException());

        // Create List
        this.lObjectsList = new LinkedList<cObject>();

        // Log
        Gdx.app.log("cWorld", "Initialised");

    }

    /** Fügt ein Object zur Renderliste hinzu */
    public void addObject(cObject oObj) {

        // Check
        if (oObj!=null) {
            this.lObjectsList.add(oObj);

            // Check if the world is set
            if (oObj.oWorld==null)
                oObj.oWorld = this;

            Gdx.app.log("cWorld", "Object added !");
        } else {
            Gdx.app.error("cWorld", "Unknown object instance !");
            return;
        }
    }

    /** Führt ein update für die Welt aus */
    public void update(float fDelta) {

        // Zum schluss müssen einige Input events zurück gesetzt werden
        if (this.oInput != null)
            this.oInput.ClearInput();

    }

    /** Führt für alle Objekte ein Update aus */
    public void updateObj(float delta) {

        // List Iterator
        ListIterator<cObject> listEnum = this.lObjectsList.listIterator();

        // Update All elements
        while (listEnum.hasNext()) {

            // Get Object
            cObject objTMP = listEnum.next();

            // check if ref is not null
            if (objTMP != null) {
                objTMP.update(delta, (int)this.oScreen.getShape().getWidth(), (int)this.oScreen.getShape().getHeight());
            }

        }
    }

    /** Gibt die aktuelle Objekt liste zurück */
    public LinkedList<cObject> getList() {
        return this.lObjectsList;
    }

    /** Gibt den Aktuellen Screen zurück */
    public cScreen getScreen() {
        return this.oScreen;
    }

    /** Gibt den Aktiven renderer Zurück */
    public cRenderer getRenderer() {
        return this.oRenderer;
    }

    /** Gibt den Aktiven renderer Zurück */
    public cInput getInput() {
        return this.oInput;
    }

    /** Fragt ab ob eine Taste der Tastatur oder der Maus gedrückt wurde */
    public boolean buttonDown(int iKey) {

        // Check key bounds
        if (iKey<0 || iKey >255) {
            Gdx.app.error("cWorld->buttonDown", "Key out of Bounds");
            return false;
        }

        // Return
        return this.oInput.baKeyDown[iKey];

    }
    public boolean mouseDown(int iKey) {

        // Check key bounds
        if (iKey<0 || iKey >cInput.MAX_MOUSE_BUTTONS) {
            Gdx.app.error("cWorld->mouseDown", "Mousekey out of Bounds");
            return false;
        }

        // Return
        return this.oInput.baMouseButtonDown[iKey];

    }

    /** Fragt ab ob eine Taste der Tastatur oder der Maus losgelassen wurde */
    public boolean buttonUp(int iKey) {

        // Check key bounds
        if (iKey<0 || iKey >255) {
            Gdx.app.error("cWorld->buttonUp", "Key out of Bounds");
            return false;
        }

        // Return
        return this.oInput.baKeyUp[iKey];

    }
    public boolean mouseUp(int iKey) {

        // Check key bounds
        if (iKey<0 || iKey >cInput.MAX_MOUSE_BUTTONS) {
            Gdx.app.error("cWorld->mouseDown", "Mousekey out of Bounds");
            return false;
        }

        // Return
        return this.oInput.baMouseButtonDown[iKey];

    }

    /** Gibt die Aktuelle Maus Position X zurück */
    public int getMouseX() {
        return this.oInput.iMouseX;
    }

    /** Gibt die Aktuelle Maus Position Y zurück */
    public int getMouseY() {
        return this.oInput.iMouseY;
    }

    /** Fragt ab ob das Mausrad bewegt wird, gibt entweder 1 für vorscrollen,  -1 für zurückscrollen oder 0 für kein scrollen zurück */
    public byte getMouseZ() {
        return this.oInput.bScroll;
    }

    /** Fragt den Char der gedrückten Taste ab (bsp. W gibt 'W' zurück) */
    public char getLastCharKeyPressed() {
        return this.oInput.cLastKey;
    }

    /** Löscht alle elemente */
    public void dispose() {

        // Lösche alle referenzierten klassen (Renderer, Screen, Input ...)
        if (this.oRenderer != null)
            this.oRenderer.dispose();

        if (this.oScreen != null)
            this.oScreen.dispose();

        if (this.oInput != null)
            this.oInput.dispose();

        // Lösche alle Objekte
        this.lObjectsList.clear();
        this.lObjectsList = null;
    }

}
