package com.gamemonkeys.ape.sources.math;

import com.badlogic.gdx.graphics.Color;

/**
 * @author Tony
 * @version 0.0.4
 */
public class cColor {
    /**
     * Die Klasse beinhaltet die einzelen Farbwerte (R, G, B, A) für eine Farbe
     * mithilfe der Klasse kann eine Farbe einfach übergeben werden.
     *
     * Es werden immer int RGB Werte übergeben dh. von 0 - 255
     * wobei 0 = Schwarz (Durchsichtig bei Alpha)
     * und 255 = Weiß    (Undurchsichtig bei Alpha)
     */

    // Statische Farben
    public static final cColor COLOR_WHITE = new cColor(255, 255, 255);
    public static final cColor COLOR_BLACK = new cColor(0, 0, 0);
    public static final cColor COLOR_RED = new cColor(255, 0, 0);
    public static final cColor COLOR_GREEN = new cColor(0, 255, 0);
    public static final cColor COLOR_BLUE = new cColor(0, 0, 255);
    public static final cColor COLOR_ORANGE = new cColor(255, 128, 0);
    public static final cColor COLOR_YELLOW = new cColor(255, 255, 0);
    public static final cColor COLOR_MINT_GREEN = new cColor(191, 255, 0);
    public static final cColor COLOR_CYAN = new cColor(0, 255, 255);
    public static final cColor COLOR_BEIGE = new cColor(245, 245, 220);
    public static final cColor COLOR_VIOLET = new cColor(138, 43, 226);
    public static final cColor COLOR_BROWN = new cColor(165, 42, 42);
    public static final cColor COLOR_CORAL = new cColor(255, 127, 80);
    public static final cColor COLOR_GOLD = new cColor(184, 134, 11);
    public static final cColor COLOR_MAGENTA = new cColor(255, 0, 255);
    public static final cColor COLOR_OLIVE = new cColor(85, 107, 47);
    public static final cColor COLOR_TURQUOISE = new cColor(0, 206, 209);
    public static final cColor COLOR_PINK = new cColor(255, 20, 147);
    public static final cColor COLOR_SKY_BLUE = new cColor(0, 191, 255);
    public static final cColor COLOR_FOREST_GREEN = new cColor(34, 139, 34);
    public static final cColor COLOR_LIGHT_BLUE = new cColor(173, 216, 230);
    public static final cColor COLOR_LIGHT_CORAL = new cColor(240, 128, 128);
    public static final cColor COLOR_PURPLE = new cColor(128, 0, 128);

    // Attribute
    public float fRed;
    public float fGreen;
    public float fBlue;
    public float fAlpha;

    /** Constructor : Initialisiert das Object (new cColor(...) */
    public cColor() {

        // Create default Color -> Black & no Alpha
        this.fRed = 0.0f;
        this.fGreen = 0.0f;
        this.fBlue = 0.0f;
        this.fAlpha = 1.0f;

    }
    public cColor(int iGrey) {

        // Set the color
        this.fRed = iGrey / 255.0f;
        this.fGreen = iGrey / 255.0f;
        this.fBlue = iGrey / 255.0f;
        this.fAlpha = 1.0f;
    }
    public cColor(int iGrey, int iAlpha) {

        // Set the color
        this.fRed = iGrey / 255.0f;
        this.fGreen = iGrey / 255.0f;
        this.fBlue = iGrey / 255.0f;
        this.fAlpha = iAlpha / 255.0f;
    }
    public cColor(int iRed, int iGreen, int iBlue) {

        // Set the color
        this.fRed = iRed / 255.0f;
        this.fGreen = iGreen / 255.0f;
        this.fBlue = iBlue / 255.0f;
        this.fAlpha = 1.0f;
    }
    public cColor(int iRed, int iGreen, int iBlue, int iAlpha) {

        // Set the color
        this.fRed = iRed / 255.0f;
        this.fGreen = iGreen / 255.0f;
        this.fBlue = iBlue / 255.0f;
        this.fAlpha = iAlpha / 255.0f;
    }

    /** Konvertierung libGdx Color <-> cColor */
    public cColor fromColor(Color oCol) {
        cColor tmp = new cColor();
        tmp.fRed = oCol.r;
        tmp.fGreen = oCol.g;
        tmp.fBlue = oCol.b;
        tmp.fAlpha = oCol.a;
        return tmp;
    }
    public Color toColor() {
        Color tmp = new Color();
        tmp.r = this.getRed();
        tmp.g = this.getGreen();
        tmp.b = this.getBlue();
        tmp.a = this.getAlpha();
        return tmp;
    }
    public Color toColor(cColor oCol) {
        Color tmp = new Color();
        tmp.r = oCol.getRed();
        tmp.g = oCol.getGreen();
        tmp.b = oCol.getBlue();
        tmp.a = oCol.getAlpha();
        return tmp;
    }

    /** Setzt den Farbwert der aktuellen Farbe */
    public void setColor(cColor oCol) {
        this.fRed = oCol.getRed();
        this.fGreen = oCol.getGreen();
        this.fBlue = oCol.getBlue();
        this.fAlpha = oCol.getAlpha();
    }
    public void setColor(int iRed, int iGreen, int iBlue) {
        // Set the color
        this.fRed = iRed / 255.0f;
        this.fGreen = iGreen / 255.0f;
        this.fBlue = iBlue / 255.0f;
        this.fAlpha = 0.0f;
    }
    public void setColor(int iRed, int iGreen, int iBlue, int iAlpha) {
        // Set the color
        this.fRed = iRed / 255.0f;
        this.fGreen = iGreen / 255.0f;
        this.fBlue = iBlue / 255.0f;
        this.fAlpha = iAlpha / 255.0f;
    }

    /** Kopiert diese Farbe und gibt eine neue Instanz zurück */
    public cColor copy() {
        return (new cColor(this.getRedi(), this.getGreeni(), this.getBluei(), this.getAlphai()));
    }

    /** Für Debug-Zwecke */
    public String toString() {
        return ("Color [R, G, B, A] ["+this.getRedi() + ", " + this.getGreeni() + ", " + this.getBluei() + ", " +this.getAlphai()+"]");
    }

    // Getter
    public float getRed() {
        return this.fRed;
    }
    public float getGreen() {
        return this.fGreen;
    }
    public float getBlue() {
        return this.fBlue;
    }
    public float getAlpha() {
        return this.fAlpha;
    }

    public int getRedi() {
        return (int)this.fRed*255;
    }
    public int getGreeni() {
        return (int)this.fGreen*255;
    }
    public int getBluei() {
        return (int)this.fBlue*255;
    }
    public int getAlphai() {
        return (int)this.fAlpha*255;
    }

    // Setter
    public void setRed(int iValue) {
        this.fRed = iValue / 255.0f;
    }
    public void setGreen(int iValue) {
        this.fGreen = iValue / 255.0f;
    }
    public void setBlue(int iValue) {
        this.fBlue = iValue / 255.0f;
    }
    public void setAlpha(int iValue) {
        this.fAlpha = iValue / 255.0f;
    }
}