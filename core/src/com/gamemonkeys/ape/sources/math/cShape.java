package com.gamemonkeys.ape.sources.math;

/**
 * @author Paul
 * @version 0.0.3
 */
public class cShape {
    /**
     * Die Klasse bietet einen einfachen zugriff auf 2 Variablen
     * X / Y der für berechnungen oder einfache Positionsangaben im 2D Raum dient.
     */

    // Attribute
    public float fWidth;
    public float fHeight;

    /** Konstruktor : Initialisiert das Object (new cShape(...) */
    public cShape() {
        this.fWidth = 0.0f;
        this.fHeight = 0.0f;
    }
    /** Erstellt ein Shape wo fWidth & fHeight den selben Wert haben **/
    public cShape(float fValue) {
        this.fWidth = fValue;
        this.fHeight = fValue;
    }
    public cShape(float fWidth, float fHeight) {
        this.fWidth = fWidth;
        this.fHeight = fHeight;
    }

    // Methods
    /** Ein shape wird mit dem aktuellen Addiert
     * @return Neues cShape object
     **/
    public cShape Add(cShape oShape) {

        // Create new
        cShape tmpShape = new cShape();

        // Set attributes
        tmpShape.fWidth = this.fWidth + oShape.fWidth;
        tmpShape.fHeight = this.fHeight + oShape.fHeight;

        // Return the new shape
        return tmpShape;

    }

    /** Setzt die Größe des shapes */
    public void setShape(cShape oVal) {
        this.fWidth = oVal.getWidth();
        this.fHeight = oVal.getHeight();
    }
    public void setShape(float fWidth, float fHeight) {
        // Set Attributes
        this.fWidth = fWidth;
        this.fHeight = fHeight;
    }

    /** Tauscht Width & Height und gibt diese instanz zurück */
    public cShape swap() {
        float tmp = this.fWidth;
        this.fWidth = this.fHeight;
        this.fHeight = tmp;
        return this;
    }

    // Getter
    public float getWidth() {
        return this.fWidth;
    }
    public float getHeight() {
        return this.fHeight;
    }

    // Setter
    public void setWidth(float fValue) {
        this.fWidth = fValue;
    }
    public void setHeight(float fValue) {
        this.fHeight = fValue;
    }

    // Debug
    public String toString() {
        return ("Width : " + this.fWidth + " Height : " + this.fHeight);
    }
}
