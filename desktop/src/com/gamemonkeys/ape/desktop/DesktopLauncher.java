package com.gamemonkeys.ape.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gamemonkeys.ape.Ape;

public class DesktopLauncher {
	public static void main (String[] arg) {

		// Creating the Application Config
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		//config.useGL30 = true;
		config.title = "Ape - Test 1";
		config.width = 800; //1680;
		config.height = 480; //1050;
		config.resizable = false;

		new LwjglApplication(new Ape(), config);
	}
}
